#!/usr/bin/env python3
from __future__ import print_function
import boto3
import logging
from pprint import pformat, pprint
import json
import os
import sys
import time


'''
Lambda handler to handle incoming SNS messages to deal with ECS autoscaling draining.

The purpose of this script is to inform ECS that an instance is about to be terminated by setting it to DRAINING mode.
After the tasks are drained the instance can finally be terminated. Without this, the task will be ungracefully murdered after which ECS has to figure out that it has disappered ;)

Infra setup:
- ASG has lifecyclehook which sends SNS on autoscaling:EC2_INSTANCE_TERMINATING
- ASG has notification topics for autoscaling:EC2_INSTANCE_LAUNCH/autoscaling:EC2_INSTANCE_LAUNCH_ERROR/autoscaling:EC2_INSTANCE_TERMINATE/autoscaling:EC2_INSTANCE_TERMINATE_ERROR
- This lambda is hooked to SNS topic

Working:
Lambda is called when EC2_INSTANCE_TERMINATING lifecycle triggers.
Next the instance's clustername is determined, after which the ECS container instance id is determined, which is set to DRAINING mode.
While (tasks still running): reschedule lambda
Finally when all tasks are gone the CONTINUE action is returned and the instance is terminated.

See http://docs.aws.amazon.com/AmazonECS/latest/developerguide/container-instance-draining.html and https://aws.amazon.com/de/blogs/compute/how-to-automate-container-instance-draining-in-amazon-ecs/ for more technical details.
'''


class ECSInstanceDrainer:
    def __init__(self, loglevel=logging.INFO):
        self.configure_logger(loglevel)
        self.session = boto3.session.Session()
        self.ec2 = self.session.client(service_name='ec2')
        self.ecs = self.session.client(service_name='ecs')
        self.asg = self.session.client('autoscaling')
        self.sns = self.session.client('sns')

    def configure_logger(self, loglevel=logging.INFO, logfile=None):
        logger = logging.getLogger()
        logger.setLevel(loglevel)
        format = "[%(filename)30s:%(funcName)20s():%(lineno)4s:%(levelname)-7s] %(message)s"
        if logfile and logfile is not None:
            logging.basicConfig(level=loglevel,
                                filename=logfile,
                                filemode='w',
                                format=format)
        else:
            logging.basicConfig(level=loglevel,
                                format=format)
        logging.getLogger('boto3').setLevel(logging.WARNING)
        logging.getLogger('botocore').setLevel(logging.WARNING)

    """Publish SNS message to trigger lambda again.
            :param message: To repost the complete original message received when ASG terminating event was received.
            :param topicARN: SNS topic to publish the message to.
    """
    def publishToSNS(self, message, topicArn):
        logging.info("Publishing to SNS topic {}".format(topicArn))
        snsResponse = self.sns.publish(
            TopicArn=topicArn,
            Message=json.dumps(message),
            Subject='Publishing SNS message to invoke lambda again..'
        )
        logging.debug("SNS response: {}".format(pformat(snsResponse)))
        return True

    def getRunningTasks(self, containerId):
        try:
            res = self.ecs.describe_container_instances(cluster=self.clusterName, containerInstances=[self.containerId])
            count = res['containerInstances'][0]['runningTasksCount']
        except Exception as e:
            logging.warning("Error getting running task count on instance, maybe it died: {}".format(pformat(e)))
            count = 0
        return count

    def getInstanceInfo(self, instanceId):
        '''
            This function retrieves a lot of info based on given instanceId:
                ECS cluster it's in
                Tasks running on instance
                Status
                etc
            Most is stored in self for later usage.
        '''
        # First list clusters
        cluster_paginator = self.ecs.get_paginator('list_clusters')
        cluster_list = cluster_paginator.paginate().build_full_result()
        self.clusters = self.ecs.describe_clusters(clusters=cluster_list['clusterArns'])['clusters']

        for cluster in self.clusters:
            cluster['instances'] = {}
            logging.debug("Getting cluster instances for cluster {} -- should have {}".format(cluster['clusterName'], cluster['registeredContainerInstancesCount']))
            instance_paginator = self.ecs.get_paginator('list_container_instances')
            instance_list = instance_paginator.paginate(
                cluster=cluster['clusterName']
            ).build_full_result()
            logging.debug(pformat(instance_list))
            if instance_list['containerInstanceArns']:
                cluster['instances'] = self.ecs.describe_container_instances(cluster=cluster['clusterName'], containerInstances=instance_list['containerInstanceArns'])
            else:
                cluster['instances'] = {'containerInstances': []}

            for instance in cluster['instances']['containerInstances']:
                if instance['ec2InstanceId'] == instanceId:
                    instance['clusterName'] = cluster['clusterName']
                    self.clusterName = cluster['clusterName']
                    self.containerId = instance['containerInstanceArn']
                    logging.info("Found instance {} in cluster {} status {} with {} running tasks".format(instanceId, self.clusterName, instance['status'], instance['runningTasksCount']))
                    self.instance = instance
                    return instance
                else:
                    logging.debug("Instance {} does not match {} in cluster {}".format(instance['ec2InstanceId'], instanceId, cluster['clusterName']))
        # Get some debug details
        msg = ""
        for cluster in self.clusters:
            instancelist = [d['ec2InstanceId'] for d in cluster['instances']['containerInstances']]
            msg = msg + "[{}] -> ({})\n".format(cluster['clusterName'], ",".join(instancelist))
        logging.info("Instance {} not found in any cluster?! Clusters details:\n{}".format(instanceId, msg))
        return None

    def setDraining(self):
        # Check if the instance state is set to DRAINING. If not, set it, so the ECS Cluster will handle de-registering instance, draining tasks and draining them
        containerStatus = self.instance['status']
        if containerStatus == 'DRAINING':
            logging.info("Instance {} with Container ID {} is already draining tasks".format(self.instanceId, self.containerId))
            return False
        logging.info("Setting instance {} with container ID {} status to DRAINING...".format(self.instanceId, self.containerId))
        ecsResponse = self.ecs.update_container_instances_state(cluster=self.clusterName, containerInstances=[self.containerId], status='DRAINING')
        logging.debug("Set draining response: {}".format(pformat(ecsResponse)))
        return True

    def completeLifecycleHook(self):
        try:
            logging.debug("Trying to complete lifecycleHook...")
            response = self.asg.complete_lifecycle_action(
                LifecycleHookName=self.lifecycleHookName,
                AutoScalingGroupName=self.asgGroupName,
                LifecycleActionResult='CONTINUE',
                InstanceId=self.instanceId
            )
            logging.info("Response received from complete_lifecycle_action: {}".format(pformat(response)))
        except Exception as e:
            logging.warning("Exception during lifecycle complete action: {}".format(pformat(e)))
            return False
        return True

    def handle(self, event, context):
        logging.info("ASG Lifecycle Lambda received the event {}".format(pformat(event)))
        self.event = event
        self.context = context

        # Check for garbage
        if 'autoscaling:TEST_NOTIFICATION' in self.event:
            logging.info("Got autoscaling:TEST_NOTIFICATION in event -- ignoring.")
            return {}

        # Pull the real message out of the event
        try:
            self.line = self.event['Records'][0]['Sns']['Message']
            self.message = json.loads(self.line)
            if 'event' in self.message and self.message['event'] == 'autoscaling:TEST_NOTIFICATION':
                logging.info("Got autoscaling:TEST_NOTIFICATION in event -- ignoring.")
                return {}
            self.instanceId = self.message['EC2InstanceId']
            self.asgGroupName = self.message['AutoScalingGroupName']
            self.snsArn = self.event['Records'][0]['EventSubscriptionArn']
            self.topicArn = self.event['Records'][0]['Sns']['TopicArn']
        except Exception as e:
            logging.error("Could not handle supplied event, error was: {}".format(pformat(e)))
            return None

        self.lifecycleHookName = None
        self.clusterName = None
        self.containerId = None

        logging.debug("Records: {}".format(pformat(self.event['Records'][0])))
        logging.debug("SNS: {}".format(pformat(event['Records'][0]['Sns'])))
        logging.debug("Message: {}".format(pformat(self.message)))
        logging.debug("Ec2 Instance Id {} in ASG {}".format(self.instanceId, self.asgGroupName))
        logging.debug("SNS ARN {}, topic {}".format(self.snsArn, self.topicArn))

        if 'LifecycleHookname' in self.message:
            logging.info("AWS CLI command to force this termination: aws autoscaling complete-lifecycle-action --auto-scaling-group-name '{}' --lifecycle-action-result CONTINUE --instance-id '{}' --lifecycle-hook-name '{}'".format(self.asgGroupName, self.instanceId, self.message['LifecycleHookname']))

        # See why we were called
        if 'LifecycleTransition' in self.message.keys():
            logging.debug("LifecycleTransition Message from Autoscaling: {}".format(self.message['LifecycleTransition']))
            if self.message['LifecycleTransition'].find('autoscaling:EC2_INSTANCE_TERMINATING') > -1:
                # Get lifecycle hook name
                self.lifecycleHookName = self.message['LifecycleHookName']
                logging.debug("Setting lifecycle hook name {}".format(self.lifecycleHookName))

                res = self.getInstanceInfo(self.instanceId)  # Figures out 'status', 'cluster' and 'task_list'
                if res is None:
                    logging.warning("WARNING: Failed to get instance info for instance {} -- TROUBLE -- completing lifecyclehook for you though...".format(self.instanceId))
                    self.completeLifecycleHook()
                    return {}

                # Set DRAINING and check if there are any tasks running on the instance
                if self.instance['status'] != 'DRAINING':
                    logging.info("Instance status is {} -> setting DRAINING".format(self.instance['status']))
                    self.setDraining()

                containerId = self.instance['containerInstanceArn']
                tasksRunning = self.getRunningTasks(containerId)
                logging.info("Still running {} tasks on container id {}".format(tasksRunning, containerId))

                # Wait a little and call ourselves again if we're waiting for drainage, or finish if we're done.
                if tasksRunning > 0:
                    while tasksRunning > 0 and self.context.get_remaining_time_in_millis() > 6000:  # loop while we have time and tasks
                        logging.debug("Still running {} tasks on container id {} aka instance {} - sleeping 3 seconds".format(tasksRunning, containerId, self.instanceId))
                        time.sleep(3)
                        tasksRunning = self.getRunningTasks(containerId)
                if tasksRunning > 0:  # If we have no more time to loop retrigger ourselves
                    self.publishToSNS(self.message, self.topicArn)
                elif tasksRunning == 0:
                    logging.debug("Setting lifecycle to complete. No tasks are running on instance anymore, completing lifecycle action....")
                    self.completeLifecycleHook()
            else:
                logging.warning("Received unhandled message (this is not a termination lifecyclehook action, either add code or remove hook): {}".format(pformat(self.message)))
        else:
            logging.debug("LifecycleTransition not found in message, keys were: {}".format((pformat(self.message.keys()))))
        return {}


def lambda_handler(event, context):
    loglevel = os.environ['LogLevel'] if 'LogLevel' in os.environ else logging.INFO
    eid = ECSInstanceDrainer(loglevel)
    return eid.handle(event, context)


if __name__ == "__main__":
    e = dict()
    c = dict()
    # Commandline call, Call lambda handler
    # lambda_handler(e, c)
    # Test
    instanceId = sys.argv[1]
    eid = ECSInstanceDrainer(logging.DEBUG)
    logging.debug("Commandline run - debug mode")
    eid.getInstanceInfo(instanceId)
    logging.info(pformat(eid.getRunningTasks(eid.containerId)))
